public class Node {
    
    private Product product;
    private Node next;
    
    public Node(Product product) {
        this.product = product;
        this.next = null;
    }
    
    public Node getNext() {
        return this.next;
    }
    
    public void setNext(Node next) {
        this.next = next;
    }
    
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }
}