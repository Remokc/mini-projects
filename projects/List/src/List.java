import java.util.ArrayList;
import java.util.Date;
import java.time.LocalDate;

public class List {
    private Node head = null;
    
    public List() {
    
    }
    
    public List(Node head) {
        this.head = head;
    }
    
    public List(Product product) {
        this.head = new Node(product);
    }
    
    public Node getHead() {
        return this.head;
    }
    
    public Node atIndex(int index) {
        Node currNode = this.head;
        
        for (int currIndex = 0; currIndex < index; currIndex++) {
            currNode = currNode.getNext();
        }
        
        return currNode;
    }
    
    public void append(Product product) {
        Node currNode = this.head;
        
        
        while (null != currNode && null != currNode.getNext()) {
            currNode = currNode.getNext();
        }
    
        if (null == currNode) {
            this.head = new Node(product);
        } else {
            currNode.setNext(new Node(product));
        }
    }
    
    public void append(Product product, int index) {
        Node currNode = this.head;
        
        for (int currIndex = 0; currIndex < index; currIndex++) {
            currNode = currNode.getNext();
        }
        
        Node temp = currNode.getNext();
        currNode.setNext(new Node(product));
        currNode.getNext().setNext(temp);
    }
    
    public void remove(int index) {
        Node currNode = this.head;
    
        for (int currIndex = 0; currIndex < index; currIndex++) {
            currNode = currNode.getNext();
        }
    
        try {
            currNode.setNext(currNode.getNext().getNext());
        } catch (Exception e) {
            currNode.setNext(null);
        }
    }
    
    public void deleteAll() {
        this.head = null;
    }
    
    public long getTotalPrice() {
        long totalPrice = 0;
        Node currNode = this.head;
        
        while (null != currNode) {
            totalPrice += currNode.getProduct().price();
            currNode = currNode.getNext();
        }
    
        return totalPrice;
    }
    
    public String getCheapestProduct() {
        Product cheapest = this.head.getProduct();
        Node currNode = this.head;
        
        while (null != currNode) {
            if (currNode.getProduct().price() < cheapest.price()) {
                cheapest = currNode.getProduct();
            }
            currNode = currNode.getNext();
        }
        
        return cheapest.name();
    }
    
    public List getExpired() {
        List expired = new List();
        Node currNode = this.head;
        
        while (null != currNode) {
            if (currNode.getProduct().expiration().before(new Date())) {
                expired.append(currNode.getProduct());
            }
            currNode = currNode.getNext();
        }
        
        return expired;
    }
}
