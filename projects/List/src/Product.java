import java.util.Date;

public class Product {
    private int price;
    private String name;
    private Date expiration;
    
    public Product(int price, String name, Date expiration) {
        this.price = price;
        this.name = name;
        this.expiration = expiration;
    }
    
    public int price() {
        return price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    
    public String name() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Date expiration() {
        return expiration;
    }
    
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
    
}
