import java.util.HashMap;
public class Hotel {
    private HashMap<Integer, String> rooms = new HashMap<Integer, String>();
    
    public boolean checkIn(int roomNumber, String guestName) {
        if (!guestName.equals("") && !this.isRoomOccupied(roomNumber)) {
            this.rooms.put(roomNumber, guestName);
            return true;
        }
        
        return false;
    }
    
    public boolean checkOut(int roomNumber) {
        if (this.isRoomOccupied(roomNumber)) {
            this.rooms.remove(roomNumber);
            return true;
        }
        
        return false;
    }
    
    public boolean isRoomOccupied(int roomNumber) {
        return this.rooms.containsKey(roomNumber);
    }
    
    public void printFloor(int floor) {
        boolean isOccupied = false;
        
        for (Integer roomNumber : this.rooms.keySet()) {
            if (floor == roomNumber / 100) {
                isOccupied = true;
                System.out.printf("%d\t: %s\n", roomNumber, this.rooms.get(roomNumber));
            }
        }
        
        if (!isOccupied) {
            System.out.printf("There are no occupied rooms in floor %d", floor);
        }
    }
    
    public void print() {
        if (!this.rooms.isEmpty()) {
            for (Integer roomNumber : this.rooms.keySet()) {
                System.out.printf("%d\t: %s\n", roomNumber, this.rooms.get(roomNumber));
            }
        } else {
            System.out.println("The hotel is empty.");
        }
    }
}
