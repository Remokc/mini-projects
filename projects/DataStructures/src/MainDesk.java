import java.util.Scanner;

public class MainDesk {
    static final int CHECKIN = 1;
    static final int CHECKOUT = 2;
    static final int PRINT_FLOOR = 3;
    static final int PRINT_HOTEL = 4;
    static final int EXIT = 5;
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Hotel hotel = new Hotel();
        
        printMenu();
        int choice = Integer.parseInt(scanner.nextLine());
        
        while (EXIT != choice) {
            int roomNumber;
            
            switch (choice) {
                case CHECKIN:
                    System.out.print("Enter desired room number and name: ");
                    String[] input = scanner.nextLine().split(" ");
                    roomNumber = Integer.parseInt(input[0]);
                    if (hotel.checkIn(roomNumber, input[1])) {
                        System.out.println("Check in succeeded");
                    } else {
                        System.out.println("Unable to check in");
                    }
                    
                    break;
                case CHECKOUT:
                    System.out.print("Enter room to checkout from: ");
                    roomNumber = Integer.parseInt(scanner.nextLine());
                    if (hotel.checkOut(roomNumber)) {
                        System.out.println("Goodbye.");
                    } else {
                        System.out.println("This room is empty, checkout failed");
                    }
                    
                    break;
                case PRINT_FLOOR:
                    System.out.print("Enter floor number: ");
                    int floorNumber = Integer.parseInt(scanner.nextLine());
                    hotel.printFloor(floorNumber);
                    
                    break;
                case PRINT_HOTEL:
                    hotel.print();
                    
                    break;
                default:
                    break;
            }
            
            printMenu();
            choice = Integer.parseInt(scanner.nextLine());
        }
    }
    
    public static void printMenu() {
        System.out.printf("""
                Welcome to the main desk, please choose an option:
                %d. Check in to a room
                %d. Checkout from a room
                %d. Print floor occupation
                %d. Print hotel occupation
                %d. Exit\n""", CHECKIN, CHECKOUT, PRINT_FLOOR, PRINT_HOTEL, EXIT);
    }
}
