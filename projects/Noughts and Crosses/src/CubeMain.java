import java.util.Scanner;
import java.io.*;

public class CubeMain {
    static final Scanner scanner = new Scanner(System.in);
    static int turn = 1;
    
    public static void main(String[] args) {
        final int LOAD = 1;
        final int NEW = 2;
        final int EXIT = 3;
        
        Cube[][] board = new Cube[3][3];
        boolean stop = false;
        boolean winner = false;
    
        printMenu();
        int choice = Integer.parseInt(scanner.nextLine());
        
        while (EXIT != choice) {
            switch (choice) {
                case LOAD:
                    System.out.print("Please enter the path of the savefile: ");
                    String path = scanner.nextLine();
                    board = loadBoard(path);
                    printBoard(board);
                    
                    break;
                case NEW:
                    initBoard(board);
                    System.out.println("To save & exit at any point enter a space.");
                    printBoard(board);
                    
                    break;
                default:
                    break;
            }
            
            stop = false;
            winner = false;
            
            while (!stop && !winner) {
                turn = (turn + 1) % 2;
                System.out.printf("Enter next move(%c): ", 0 == turn ? 'x' : 'o');
                String inputStr = scanner.nextLine();
                String[] input;
                int row = 0;
                int col = 0;
                
                if (inputStr.equals(" ")) {
                    System.out.print("Enter path to save: ");
                    String path = scanner.nextLine();
                    writeBoard(path, board, turn);
                    stop = true;
                } else {
                    input = inputStr.split("[ ,]+");
                    row = Integer.parseInt(input[0]);
                    col = Integer.parseInt(input[1]);
                    
                    if (' ' == board[row][col].getSign()) {
                        if (0 == turn % 2) {
                            board[row][col].setSign('x');
                        } else {
                            board[row][col].setSign('o');
                        }
                        
                    } else {
                        System.out.println("Invalid choice, your turn is skipped.");
                    }
                }
    
                printBoard(board);
                winner = isWinner(board, board[row][col]);
            }
            
            if (winner) {
                System.out.printf("The winner is player%d\n", turn);
            }
            
            printMenu();
            choice = Integer.parseInt(scanner.nextLine());
        }
    }
    
    public static void printMenu() {
        System.out.println("Choose an option:\n" +
                           "1. Continue a previous game\n" +
                           "2. Start a new game\n" +
                           "3. Exit");
    }
   
    public static void printBoard(Cube[][] board) {
        String spacer = "  " + "-".repeat(13);
        
        System.out.println("  | 0 | 1 | 2 |");
        for (int row=0; row < board.length; row++) {
            String currRow = row + " ";
            
            for (int col=0; col < board[0].length; col++) {
                currRow += String.format("| %c ", board[row][col].getSign());
            }
            
            System.out.println(spacer);
            System.out.println(currRow + "|");
        }
    }
    
    public static void initBoard(Cube[][] board) {
        for (int row=0; row < board.length; row++) {
            for (int col=0; col < board[0].length; col++) {
                board[row][col] = new Cube(row, col);
            }
        }
    }
    
    public static Cube[][] loadBoard(String path) {
        Cube[][] board = new Cube[3][3];
        
        try (ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(path))) {
            board = (Cube[][]) objectInputStream.readObject();
            turn = (int) objectInputStream.readObject() + 1;
        } catch (EOFException e) {
            System.out.println("Reached end of file.");
        } catch (FileNotFoundException e) {
            System.out.println("Savefile not found, returning to main menu.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        return board;
    }
    
    public static void writeBoard(String path, Cube[][] board, int turn) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(path))) {
            objectOutputStream.writeObject(board);
            objectOutputStream.writeObject(turn);
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static boolean isWinner(Cube[][] board, Cube lastMove) {
        if (lastMove.getCol() == lastMove.getRow()) {
            return board[0][0].getSign() == board[1][1].getSign()
                   && board[0][0].getSign() == board[2][2].getSign();
        } else if (2 == lastMove.getCol() + lastMove.getRow()) {
            return board[0][2].getSign() == board[1][1].getSign()
                   && board[0][2].getSign() == board[2][0].getSign();
        }
        
        return board[lastMove.getRow()][0].getSign() == board[lastMove.getRow()][1].getSign()
               && board[lastMove.getRow()][0].getSign() == board[lastMove.getRow()][2].getSign()
               || board[0][lastMove.getCol()].getSign() == board[1][lastMove.getCol()].getSign()
                  && board[0][lastMove.getCol()].getSign() == board[2][lastMove.getCol()].getSign();
    }
}
