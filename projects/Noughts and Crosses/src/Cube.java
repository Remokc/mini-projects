import java.io.*;

public class Cube implements Serializable {
    private int row;
    private int col;
    private char sign;
    
    public Cube() {
        this.setSign(' ');
    }
    
    public Cube(int row, int col) {
        this.row = row;
        this.col = col;
        this.setSign(' ');
    }
    
    public Cube(int row, int col, char sign) {
        this.row = row;
        this.col = col;
        this.setSign(sign);
    }
    
    public void setSign(char sign) {
        this.sign = sign;
    }
    
    public char getSign() {
        return this.sign;
    }
    
    public int getRow() {
        return this.row;
    }
    
    public int getCol() {
        return this.col;
    }
}
